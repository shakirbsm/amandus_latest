/**
 * @defgroup Laplacegroup Laplace
 * @ingroup Examples
 * 
 * <h3> For beginners </h3>
 *
 * The following list suggests in wich order read the source files:
 * <ol>
 *   <li> laplace.cc
 *   <li> laplace/polynomial.cc
 * </ol>
 *
 */
